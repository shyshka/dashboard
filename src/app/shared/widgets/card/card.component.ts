import { Component, Input, OnInit } from '@angular/core';
import * as Highcharts from 'highcharts';
import HC_exporting from 'highcharts/modules/exporting';

@Component({
  selector: 'app-widget-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss'],
})
export class CardComponent implements OnInit {
  @Input() label: string;

  @Input() total: string;

  @Input() percentage: string;

  Highcharts = Highcharts;

  chartOptions = {};

  constructor() {}

  ngOnInit(): void {
    this.initChartOptions();
  }

  initChartOptions() {
    this.chartOptions = {
      chart: {
        type: 'area',
        backgroundColor: null,
        borderWidth: 0,
        margin: [2, 2, 2, 2],
        height: 60,
      },
      legend: {
        enabled: false,
      },
      title: {
        text: null,
      },
      subtitle: {
        text: null,
      },
      yAxis: {
        visible: false,
      },
      xAxis: {
        labels: {
          visible: false,
        },
        title: {
          text: null,
        },
        startOnTick: false,
        endOfTick: false,
        tickOptions: [],
      },
      tooltip: {
        split: true,
        outside: true,
      },
      exporting: {
        enabled: false,
      },
      credits: {
        enabled: false,
      },
      series: [{ data: [71, 78, 39, 67] }],
    };

    HC_exporting(this.Highcharts);
  }
}
